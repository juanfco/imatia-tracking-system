package com.imatia.prueba.trackingsystem.api;

import com.imatia.prueba.trackingsystem.model.OrderTrackingDto;
import com.imatia.prueba.trackingsystem.model.OrderTrackingsDto;
import com.imatia.prueba.trackingsystem.model.service.TrackingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequestMapping(path = "/order")
public class OrderApiController {

    @Autowired
    TrackingService trackingService;

    @PostMapping(path = "/tracking", consumes = "application/json", produces = "application/json")
    public OrderTrackingsDto orderTracking(@RequestBody OrderTrackingsDto request){
        OrderTrackingsDto response = new OrderTrackingsDto();
        response.setOrderTrackings(new ArrayList<>());
        for(OrderTrackingDto orderTrackingDto : request.getOrderTrackings()){
            OrderTrackingDto result = trackingService.transitTrackingStatus(orderTrackingDto);
            response.getOrderTrackings().add(result);
        }
        return response;
    }
}
