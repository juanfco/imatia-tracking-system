package com.imatia.prueba.trackingsystem.utils;

import com.imatia.prueba.trackingsystem.model.OrderTrackingDto;
import com.imatia.prueba.trackingsystem.model.dao.Order;

public class OrderDtoMapper {

    public static Order mapToOrder(OrderTrackingDto orderTrackingDto) {
        Order order = new Order();
        order.setOrderId(orderTrackingDto.getOrderId());
        order.setTrackingStatusId(orderTrackingDto.getTrackingStatusId());
        order.setChangeStatusDate(orderTrackingDto.getChangeStatusDate());
        return order;
    }

    public static OrderTrackingDto mapToOrderTrackingDto(Order order) {
        OrderTrackingDto orderTrackingDto = new OrderTrackingDto();
        orderTrackingDto.setOrderId(order.getOrderId());
        orderTrackingDto.setTrackingStatusId(order.getTrackingStatusId());
        orderTrackingDto.setChangeStatusDate(order.getChangeStatusDate());
        return orderTrackingDto;
    }
}
