package com.imatia.prueba.trackingsystem.model;

public enum TrackingStatusEnum {
    RECOGIDO_EN_ALMACEN(1),
    EN_REPARTO(2),
    INCIDENCIA_EN_ENTREGA(3),
    ENTREGADO(4);

    private Integer id;

    TrackingStatusEnum(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public static TrackingStatusEnum findById(Integer id){
        for(TrackingStatusEnum statusEnum : TrackingStatusEnum.values()){
            if(statusEnum.getId().equals(id)){
                return statusEnum;
            }
        }
        return null;
    }
}
