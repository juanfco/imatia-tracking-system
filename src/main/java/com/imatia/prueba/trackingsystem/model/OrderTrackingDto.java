package com.imatia.prueba.trackingsystem.model;

import lombok.Data;

import java.util.Date;

@Data
public class OrderTrackingDto {

    private Long orderId;
    private Integer trackingStatusId;
    private Date changeStatusDate;

}
