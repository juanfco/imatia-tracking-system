package com.imatia.prueba.trackingsystem.model.dao.impl;

import com.imatia.prueba.trackingsystem.model.dao.Order;
import com.imatia.prueba.trackingsystem.model.dao.OrderDao;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Supplier;

@Repository
public class OrderDaoImpl implements OrderDao {

    private Map<Long,Order> database = new LinkedHashMap();

    @Override
    public Order createOrder(Order newOrder) {
        newOrder.setCreationDate(new Date());
        getDatabase().put(newOrder.getOrderId(), newOrder);
        return newOrder;
    }

    @Override
    public Order updateOrder(Order order) {
        Order updateOrder = getDatabase().get(order.getOrderId());
        updateOrder.setTrackingStatusId(order.getTrackingStatusId());
        updateOrder.setChangeStatusDate(order.getChangeStatusDate());
        getDatabase().put(order.getOrderId(), updateOrder);
        return updateOrder;
    }

    @Override
    public Order findOrderById(Long id) {
        return getDatabase().get(id);
    }

    private Map<Long,Order> getDatabase(){
        return this.database;
    }
}
