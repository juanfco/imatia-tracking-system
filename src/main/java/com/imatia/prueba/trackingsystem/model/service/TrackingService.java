package com.imatia.prueba.trackingsystem.model.service;

import com.imatia.prueba.trackingsystem.model.OrderTrackingDto;

public interface TrackingService {
    public OrderTrackingDto transitTrackingStatus(OrderTrackingDto orderTrackingDto);
}
