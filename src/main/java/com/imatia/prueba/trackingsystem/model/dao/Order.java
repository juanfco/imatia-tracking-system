package com.imatia.prueba.trackingsystem.model.dao;

import com.imatia.prueba.trackingsystem.model.TrackingStatusEnum;
import lombok.Data;

import java.util.Date;

@Data
public class Order {
    private Long orderId;
    private Integer trackingStatusId;
    private Date creationDate;
    private Date changeStatusDate;
}
