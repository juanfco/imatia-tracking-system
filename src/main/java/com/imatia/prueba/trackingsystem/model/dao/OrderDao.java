package com.imatia.prueba.trackingsystem.model.dao;

public interface OrderDao {
    public Order createOrder(Order order);
    public Order updateOrder(Order order);
    public Order findOrderById(Long id);
}
