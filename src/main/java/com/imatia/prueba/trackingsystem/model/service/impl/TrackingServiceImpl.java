package com.imatia.prueba.trackingsystem.model.service.impl;

import com.imatia.prueba.trackingsystem.model.OrderTrackingDto;
import com.imatia.prueba.trackingsystem.model.TrackingStatusEnum;
import com.imatia.prueba.trackingsystem.model.dao.Order;
import com.imatia.prueba.trackingsystem.model.dao.OrderDao;
import com.imatia.prueba.trackingsystem.model.service.TrackingService;
import com.imatia.prueba.trackingsystem.utils.OrderDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrackingServiceImpl implements TrackingService {

    @Autowired
    OrderDao orderDao;

    @Override
    public OrderTrackingDto transitTrackingStatus(OrderTrackingDto orderTrackingDto) {
        TrackingStatusEnum statusEnum = TrackingStatusEnum.findById(orderTrackingDto.getTrackingStatusId());
        OrderTrackingDto order;
        switch (statusEnum){
            case RECOGIDO_EN_ALMACEN:
                order = createOrder(orderTrackingDto);
                break;
            case ENTREGADO:
                order = updateToFinal(orderTrackingDto);
                break;
            default:
                order = update(orderTrackingDto);
                break;
        }
        return order;
    }

    private OrderTrackingDto update(OrderTrackingDto orderTrackingDto) {
        Order order = orderDao.findOrderById(orderTrackingDto.getOrderId());
        if(order != null && !order.getTrackingStatusId().equals(TrackingStatusEnum.ENTREGADO.getId())){
            return OrderDtoMapper.mapToOrderTrackingDto(orderDao.updateOrder(OrderDtoMapper.mapToOrder(orderTrackingDto)));
        }else {
            return null;
        }
    }

    private OrderTrackingDto updateToFinal(OrderTrackingDto orderTrackingDto) {
        Order order = orderDao.findOrderById(orderTrackingDto.getOrderId());
        if(order != null && !order.getTrackingStatusId().equals(TrackingStatusEnum.ENTREGADO.getId())){
            return OrderDtoMapper.mapToOrderTrackingDto(
                    orderDao.updateOrder(OrderDtoMapper.mapToOrder(orderTrackingDto)));
        }else {
            return null;
        }
    }

    private OrderTrackingDto createOrder(OrderTrackingDto orderTrackingDto) {
        Order order = orderDao.findOrderById(orderTrackingDto.getOrderId());
        if(order == null){
            return OrderDtoMapper.mapToOrderTrackingDto(
                    orderDao.createOrder(OrderDtoMapper.mapToOrder(orderTrackingDto)));
        }else{
            return null;
        }
    }


}
