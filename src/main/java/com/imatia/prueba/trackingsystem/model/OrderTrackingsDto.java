package com.imatia.prueba.trackingsystem.model;

import lombok.Data;

import java.util.List;

@Data
public class OrderTrackingsDto {
    List<OrderTrackingDto> orderTrackings;
}
