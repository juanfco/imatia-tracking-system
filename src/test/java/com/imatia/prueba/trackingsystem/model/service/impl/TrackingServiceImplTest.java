package com.imatia.prueba.trackingsystem.model.service.impl;

import com.imatia.prueba.trackingsystem.model.OrderTrackingDto;
import com.imatia.prueba.trackingsystem.model.OrderTrackingsDto;
import com.imatia.prueba.trackingsystem.model.TrackingStatusEnum;
import com.imatia.prueba.trackingsystem.model.dao.Order;
import com.imatia.prueba.trackingsystem.model.dao.OrderDao;
import com.imatia.prueba.trackingsystem.model.service.TrackingService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;

@RunWith(MockitoJUnitRunner.class)
public class TrackingServiceImplTest {

    @Mock
    OrderDao orderDao;
    @InjectMocks
    TrackingServiceImpl service;

    @Test
    public void testTransitToState1Ok(){
        //Given
        OrderTrackingDto orderDto = new OrderTrackingDto();
        orderDto.setTrackingStatusId(TrackingStatusEnum.RECOGIDO_EN_ALMACEN.getId());
        Mockito.when(orderDao.findOrderById(anyLong())).thenReturn(null);
        Mockito.when(orderDao.createOrder(any())).thenReturn(new Order());
        //When
        OrderTrackingDto result = service.transitTrackingStatus(orderDto);
        //Then
        Assert.assertNotNull(result);
    }

    @Test
    public void testTransitToState4Ok(){
        //Given
        OrderTrackingDto orderDto = new OrderTrackingDto();
        orderDto.setTrackingStatusId(TrackingStatusEnum.ENTREGADO.getId());
        orderDto.setOrderId(1l);
        Order orderDao = new Order();
        orderDao.setTrackingStatusId(TrackingStatusEnum.EN_REPARTO.getId());
        Mockito.when(this.orderDao.findOrderById(anyLong())).thenReturn(orderDao);
        Mockito.when(this.orderDao.updateOrder(any())).thenReturn(new Order());
        //When
        OrderTrackingDto result = service.transitTrackingStatus(orderDto);
        //Then
        Assert.assertNotNull(result);
    }

    @Test
    public void testTransitToDiferentStateOk(){
        //Given
        OrderTrackingDto orderDto = new OrderTrackingDto();
        orderDto.setTrackingStatusId(TrackingStatusEnum.EN_REPARTO.getId());
        orderDto.setOrderId(1l);
        Order orderDao = new Order();
        orderDao.setTrackingStatusId(TrackingStatusEnum.RECOGIDO_EN_ALMACEN.getId());
        Mockito.when(this.orderDao.findOrderById(anyLong())).thenReturn(orderDao);
        Mockito.when(this.orderDao.updateOrder(any())).thenReturn(new Order());
        //When
        OrderTrackingDto result = service.transitTrackingStatus(orderDto);
        //Then
        Assert.assertNotNull(result);
    }

    @Test
    public void testTransitToState1Error(){
        //Given
        OrderTrackingDto orderDto = new OrderTrackingDto();
        orderDto.setTrackingStatusId(TrackingStatusEnum.RECOGIDO_EN_ALMACEN.getId());
        orderDto.setOrderId(1l);
        Order orderDao = new Order();
        orderDao.setTrackingStatusId(TrackingStatusEnum.EN_REPARTO.getId());
        Mockito.when(this.orderDao.findOrderById(anyLong())).thenReturn(orderDao);
        //When
        OrderTrackingDto result = service.transitTrackingStatus(orderDto);
        //Then
        Assert.assertNull(result);
    }

    @Test
    public void testTransitFromState4Error(){
        //Given
        OrderTrackingDto orderDto = new OrderTrackingDto();
        orderDto.setTrackingStatusId(TrackingStatusEnum.EN_REPARTO.getId());
        orderDto.setOrderId(1l);
        Order orderDao = new Order();
        orderDao.setTrackingStatusId(TrackingStatusEnum.ENTREGADO.getId());
        Mockito.when(this.orderDao.findOrderById(anyLong())).thenReturn(orderDao);
        //When
        OrderTrackingDto result = service.transitTrackingStatus(orderDto);
        //Then
        Assert.assertNull(result);
    }

}